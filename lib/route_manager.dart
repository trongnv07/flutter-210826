import 'package:flutter/material.dart';
import 'package:my_app3/product/page/product_detail.dart';
import 'package:my_app3/product/page/product_list.dart';

import 'authen/page/login.dart';
import 'home/page/home_page.dart';

const String loginPage = 'login';
const String homePage = 'home';
const String productListPage = 'product_list';
const String productDetailPage = 'product_detail';

Route<dynamic> controllerRoute(RouteSettings settings) {
  switch (settings.name) {
    case homePage:
      return MaterialPageRoute(builder: (context) => HomePage());
    case productListPage:
      return MaterialPageRoute(builder: (context) => ProductListPage());

    case productDetailPage:
      return MaterialPageRoute(builder: (context) => ProductDetailPage());
    case loginPage:
      return MaterialPageRoute(builder: (context) => LoginPage());
    default:
      throw ('this route name does not exit');
  }
}
