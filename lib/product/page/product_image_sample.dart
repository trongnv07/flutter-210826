//
// Container(
//                 width: MediaQuery.of(context).size.width,
//                 height: 250,
//                 child: ListView.builder(
//                     scrollDirection: Axis.horizontal,
//                     itemCount: productList.length,
//                     itemBuilder: (context, index) {
//                       return Container(
//                         color: (index == 1) ? Colors.red : Colors.yellow,
//                         alignment: Alignment.center,
//                         child: Column(
//                           children: [
//                             Expanded(
//                                 flex: 8,
//                                 child: Image.network(
//                                   productList[index].image.toString(),
//                                   fit: BoxFit.fitWidth,
//                                 )),
//                             Expanded(
//                               flex: 2,
//                               child: Padding(
//                                 padding: EdgeInsets.only(top: 10),
//                                 child: Text("Quang cao",
//                                     style: TextStyle(
//                                         fontSize: 18,
//                                         fontWeight: FontWeight.bold)),
//                               ),
//                             )
//                             //
//                           ],
//                         ),
//                         width: MediaQuery.of(context).size.width,
//                         height: 200,
//                       );
//                     }),
//               ),

import 'package:flutter/material.dart';

class ProductImageSample extends StatefulWidget {
  @override
  _ProductImageSampleState createState() => _ProductImageSampleState();
}

final List<Image> imgListSlider = [
  Image.network(
      'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80'),
];

class _ProductImageSampleState extends State<ProductImageSample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(),
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar(
            pinned: true,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Demo'),
            ),
          ),
          SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.teal[100 * (index % 9)],
                  child: Text('Grid Item $index'),
                );
              },
              childCount: 20,
            ),
          ),
          SliverFixedExtentList(
            itemExtent: 50.0,
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 9)],
                  child: Text('List Item $index'),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
