import 'package:flutter/material.dart';
import 'package:my_app3/authen/model/login_model.dart';
import 'package:my_app3/product/model/product_model.dart';
import 'package:my_app3/product/model/product_sort_model.dart';
import 'package:my_app3/product/page/product_image_sample.dart';
import 'package:my_app3/route_manager.dart';
import 'package:provider/provider.dart';

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

class _ProductListPageState extends State<ProductListPage> {
  Widget showProduct(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 1,
      ),
      itemCount: productList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, productDetailPage);

            // Navigator.push(
            //   context,
            //   MaterialPageRoute(builder: (context) => ProductImageSample()),
            // );
          },
          child: Container(
            color: Colors.green,
            alignment: Alignment.center,
            child: Column(
              children: [
                Expanded(
                    flex: 8,
                    child: Image.network(
                      productList[index].image.toString(),
                      fit: BoxFit.fitWidth,
                    )),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(productList[index].name.toString(),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                )
                //
              ],
            ),
            width: MediaQuery.of(context).size.width,
            //height: 200,
          ),
        );
      },
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
    );
  }

  @override
  Widget build(BuildContext context) {
    //final loginModel = Provider.of<LoginModel>(context);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Product List"), actions: [Container()]),
      endDrawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Drawer Header'),
            ),
            ListTile(
              title: const Text('Item 1'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: const Text('Item 2'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Consumer<LoginModel>(builder: (context, login, child) {
              return Container(
                child: Text("${login.getFullName()}"),
              );
            }),
            Container(
              //olor: Colors.yellow,
              height: 50,
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    //
                    onTap: () {
                      showModalBottomSheet<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              height: MediaQuery.of(context).size.height * 0.5,
                              //color: Colors.amber,

                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15)),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 50,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Sap xep theo",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold)),
                                        IconButton(
                                            onPressed: () =>
                                                Navigator.pop(context),
                                            icon: Icon(Icons.close))
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                      child: ListView.builder(
                                          scrollDirection: Axis.vertical,
                                          itemCount: productSort.length,
                                          itemBuilder: (context, index) {
                                            return Container(
                                              width: double.infinity,
                                              height: 44,
                                              child: ListTile(
                                                leading: Text(
                                                    productSort[index]
                                                        .title
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontSize: 16)),
                                                trailing: Icon(
                                                  Icons.arrow_right,
                                                  size: 30,
                                                ),
                                              ),
                                              // child: Row(
                                              //   mainAxisAlignment:
                                              //       MainAxisAlignment
                                              //           .spaceBetween,
                                              //   children: [
                                              //     Text(
                                              //         productSort[index]
                                              //             .title
                                              //             .toString(),
                                              //         style: TextStyle(
                                              //             fontSize: 16)),
                                              //     Icon(
                                              //       Icons.arrow_right,
                                              //       size: 30,
                                              //     )
                                              //   ],
                                              // ),
                                            );
                                          }))
                                ],
                              ));
                        },
                      );
                    },
                    child: Row(
                      children: [
                        Icon(Icons.sort),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: Colors.grey),
                          child: Row(
                            children: [
                              Icon(Icons.share_outlined),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Giam gia nhieu")
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _scaffoldKey.currentState?.openEndDrawer();
                        },
                        child: Container(
                          child: Row(
                            children: [
                              Icon(Icons.filter),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Bo loc")
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            showProduct(context)
          ],
        ),
      )),
    );
  }
}
