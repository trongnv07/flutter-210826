import 'package:flutter/material.dart';
import 'package:my_app3/product/model/product_model.dart';
import 'package:my_app3/product/model/product_sort_model.dart';
import 'package:my_app3/product/widget/slider_product_detail_widget.dart';

class ProductDetailPage extends StatefulWidget {
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  Widget stepper(BuildContext context) {
    return Row(
      children: [
        Text("So luong"),
        SizedBox(width: 20),
        Container(
          width: 100,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black12.withOpacity(0.1))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                    border: Border(
                        right: BorderSide(
                            color: Colors.black12.withOpacity(0.1)))),
                child: Center(
                  child: Text("-"),
                ),
              ),
              Center(
                child: Text("1"),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                    border: Border(
                        left: BorderSide(
                            color: Colors.black12.withOpacity(0.1)))),
                child: Center(
                  child: Text("+"),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget productInfo(BuildContext context) {
    return Column(
      children: [
        Text(
          "Bo bao dong tu",
          textAlign: TextAlign.start,
        ),
        ListTile(
          leading: Text("2,789,333"),
          trailing: Icon(Icons.favorite),
        ),
        Row(
          children: [
            Text("Model"),
            Text("NK 3434"),
          ],
        ),
        Row(
          children: [
            Text("Kho hang"),
            Text("NK 3434"),
          ],
        ),
        Row(
          children: [
            Text("Van chuyen"),
            Text("NK 3434"),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        CustomScrollView(slivers: <Widget>[
          SliverAppBar(
            actions: [Icon(Icons.add_shopping_cart)],
            pinned: true,
            elevation: 0.1,
            centerTitle: true,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
                title: Text('Demo'),
                background: Image.network(
                  "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                  fit: BoxFit.cover,
                )),
          ),
          SliverToBoxAdapter(
            child: productInfo(context),
          ),
          SliverToBoxAdapter(
            child: stepper(context),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  width: double.infinity,
                  height: 44,
                  child: ListTile(
                    leading: Text(productDetailSubMenu[index].title.toString(),
                        style: TextStyle(fontSize: 16)),
                    trailing: Icon(
                      Icons.arrow_right,
                      size: 30,
                    ),
                  ),
                );
              },
              childCount: productDetailSubMenu.length,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  width: double.infinity,
                  height: 44,
                  child: ListTile(
                    leading: Text(productDetailSubMenu[index].title.toString(),
                        style: TextStyle(fontSize: 16)),
                    trailing: Icon(
                      Icons.arrow_right,
                      size: 30,
                    ),
                  ),
                );
              },
              childCount: productDetailSubMenu.length,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  width: double.infinity,
                  height: 44,
                  child: ListTile(
                    leading: Text(productDetailSubMenu[index].title.toString(),
                        style: TextStyle(fontSize: 16)),
                    trailing: Icon(
                      Icons.arrow_right,
                      size: 30,
                    ),
                  ),
                );
              },
              childCount: productDetailSubMenu.length,
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 50,
            ),
          )
        ]),
        Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              //color: Colors.green,
              height: 50,
              decoration: BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.grey))),
              child: Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.chat),
                          Icon(Icons.shopping_cart),
                        ],
                      )),
                  Expanded(
                      flex: 1,
                      child: Container(
                        child: Text('Mua ngay'),
                      ))
                ],
              ),
            ))
      ],
    ));
  }
}
