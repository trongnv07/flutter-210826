class Product {
  const Product({this.productId, this.image, this.name, this.price});
  final int? productId;
  final String? image;
  final String? name;
  final int? price;
}

// product list
const List<Product> productList = const <Product>[
  const Product(
      productId: 1,
      name: "San pham A",
      price: 10000,
      image:
          "https://salt.tikicdn.com/cache/w1080/ts/banner/42/4c/60/3a35e2cb827ecd54e2dada5527a7da71.png"),
  const Product(
      productId: 1,
      name: "San pham B",
      price: 50000,
      image:
          "https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham C",
      price: 20000,
      image:
          "https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham D",
      price: 30000,
      image:
          "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80"),
  const Product(
      productId: 1,
      name: "San pham A",
      price: 10000,
      image:
          "https://salt.tikicdn.com/cache/w1080/ts/banner/42/4c/60/3a35e2cb827ecd54e2dada5527a7da71.png"),
  const Product(
      productId: 1,
      name: "San pham B",
      price: 50000,
      image:
          "https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham C",
      price: 20000,
      image:
          "https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham D",
      price: 30000,
      image:
          "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80"),
  const Product(
      productId: 1,
      name: "San pham A",
      price: 10000,
      image:
          "https://salt.tikicdn.com/cache/w1080/ts/banner/42/4c/60/3a35e2cb827ecd54e2dada5527a7da71.png"),
  const Product(
      productId: 1,
      name: "San pham B",
      price: 50000,
      image:
          "https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham C",
      price: 20000,
      image:
          "https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham D",
      price: 30000,
      image:
          "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80"),
  const Product(
      productId: 1,
      name: "San pham A",
      price: 10000,
      image:
          "https://salt.tikicdn.com/cache/w1080/ts/banner/42/4c/60/3a35e2cb827ecd54e2dada5527a7da71.png"),
  const Product(
      productId: 1,
      name: "San pham B",
      price: 50000,
      image:
          "https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham C",
      price: 20000,
      image:
          "https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80"),
  const Product(
      productId: 1,
      name: "San pham D",
      price: 30000,
      image:
          "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80")
];
