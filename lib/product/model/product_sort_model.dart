class ProductSort {
  const ProductSort({this.title, this.sort});
  final String? title;
  final String? sort;
}

const List<ProductSort> productSort = const <ProductSort>[
  const ProductSort(
    title: "Gia giam dan",
    sort: "price_desc",
  ),
  const ProductSort(
    title: "Gia tang dan",
    sort: "price_asc",
  ),
  const ProductSort(
    title: "San pham moi",
    sort: "newest",
  )
];

class ProductDetailSubMenu {
  const ProductDetailSubMenu({this.title, this.deeplink});
  final String? title;
  final String? deeplink;
}

const List<ProductDetailSubMenu> productDetailSubMenu =
    const <ProductDetailSubMenu>[
  const ProductDetailSubMenu(
    title: "Gia giam dan",
    deeplink: "myapp://promotion",
  ),
  const ProductDetailSubMenu(
    title: "Gia tang dan",
    deeplink: "myapp://promotion",
  ),
  const ProductDetailSubMenu(
    title: "San pham moi",
    deeplink: "myapp://promotion",
  )
];
