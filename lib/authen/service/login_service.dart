import 'package:my_app3/authen/model/user_model.dart';
import 'package:my_app3/networking/dio_manager.dart';
import 'package:my_app3/networking/http_utlils.dart';

class LoginService {
  //HttpUtils myHttp = new HttpUtils();

  DioManager dioManager = new DioManager();

  Future<User> handleLogin(String email, String passWord) async {
    ResponseApi? responseApi;
    User? _user = new User();

    responseApi = await dioManager.request(
        path: "user/sign-in",
        method: Method.post,
        params: {'email': email, 'password': passWord},
        onSuccess: (response) {
          print(response);
          _user = User.fromJson(response.data);
          // print(_user);

          return _user;
          //return _user;
        },
        onFailure: (json) {
          // print(json);
          return _user;
        });

    // responseApi = await myHttp.request(
    //     "user/sign-in", "post", Map(), {'email': email, 'password': passWord});

    // if (responseApi.code == 200) {
    //   if (responseApi.data != null) {
    //     // store token to local storage
    //     return User.fromJson(responseApi.data);
    //   }
    // } else {
    //   print('Request failed with status: ${responseApi.code}.');
    // }

    return _user!;
  }
}
