import 'package:flutter/cupertino.dart';

class LoginModel with ChangeNotifier {
  String? fullName;

  getFullName() => fullName;

  setFullName(String _fullName) {
    fullName = _fullName;
    notifyListeners();
  }
}
