class User {
  String? accessToken;
  String? fullName;

  User({this.accessToken, this.fullName});

  factory User.fromJson(Map<String, dynamic> json) =>
      User(accessToken: json["token"], fullName: json['name']);
}

// class UserLogin {
//   String? accessToken;
//   String? fullName;

//   UserLogin({this.accessToken, this.fullName});

// }
