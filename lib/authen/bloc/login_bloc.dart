import 'dart:async';

import 'package:my_app3/authen/model/user_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:my_app3/authen/service/login_service.dart';

class LoginBloc {
  User? _user = new User();
  LoginService loginService = new LoginService();

  final loginStream = StreamController<User>.broadcast();
  Stream<User> get loginState => loginStream.stream;

  Future<User> handleLogin(String email, String passWord) async {
    // validate username, password

    // call api, response => user info => mapping user model

    // return UI (object user model)

    // _user!.accessToken = "ajdsflkdajfla343434";
    // _user!.fullName = "Nguyen Van A";
    // loginStream.add(_user!);

    // var response = await http.post(
    //     Uri.parse('http://sanbongvinhloi.com/api/user/sign-in'),
    //     body: {'email': email, 'password': passWord});

    return await loginService.handleLogin(email, passWord);
  }
}
