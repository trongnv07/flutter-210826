import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my_app3/authen/bloc/login_bloc.dart';
import 'package:my_app3/authen/model/login_model.dart';
import 'package:my_app3/authen/model/user_model.dart';
import 'package:provider/provider.dart';

import '../../route_manager.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  String? userName;
  String? password;
  bool isLoginSuccess = false;
  bool isValidInput = false;
  bool isSecurePassword = true;
  LoginBloc? loginBloc = new LoginBloc();

  TextEditingController _usernameController = TextEditingController(text: '');
  TextEditingController _passwordController = TextEditingController();

  // final loginStream = StreamController<String>.broadcast();
  // Stream<String> get loginState => loginStream.stream;

// app compile , php //

  void validateLogin() async {
    // loading
    if (_usernameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      // set fullName;

      //loginBloc!.loginStream.add(_usernameController.text);

      loginBloc!.loginState.listen((value) {
        // if add,/dsfdsaf
        print(
            'value validateLogin: ${value.accessToken} and ${value.fullName}');

        // print('value validateLogin: ${jsonEncode(value)} ');
        // validateEmail lkasjdf@
      });

      User _user = await loginBloc!
          .handleLogin(_usernameController.text, _passwordController.text);

      print('------------');
      print(_user.fullName);

      if (_user.accessToken != null) {
        // push to home page
        Navigator.pushReplacementNamed(context, homePage);
      } else {
        // dang nhap that bai
      }

      // them, xoa, sua data of stream

      // callback function

      // loginBloc!.loginState.listen((event) {
      //   print('value of login1111: $event');
      // });
      print('---------');
      // loginBloc!.user.s.stream.listen((event) {
      //
      // });

      // setState(() {
      //   isLoginSuccess = true;
      //   isValidInput = true;
      //   userName = 'nguyenvanb';
      // });
      // _usernameController.text = userName.toString();
    } else {
      setState(() {
        isLoginSuccess = false;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loginBloc = new LoginBloc();

    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }

  Widget togglePassword() {
    return GestureDetector(
      onTap: () {
        setState(() {
          this.isSecurePassword = !isSecurePassword;
        });
      },
      child: Container(
        child:
            this.isSecurePassword ? Icon(Icons.search) : Icon(Icons.search_off),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final loginModel = Provider.of<LoginModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Login page"),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(80.0),
          child: Column(
            children: [
              Text('Login'),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Username',
                ),
                controller: _usernameController,
                //initialValue: 'ten dang nhap',
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Password',
                  suffixIcon: togglePassword(),
                ),

                obscureText: this.isSecurePassword,
                controller: _passwordController,

                // onChanged: (value) {
                //   return
                // },
                //keyboardType: TextInputType.phone,
                textInputAction: TextInputAction.continueAction,
              ),
              SizedBox(height: 24),
              // this.isLoginSuccess && this.isValidInput
              //     ? Text('$userName Dang nhap thanh cong ')
              //     : Text('$userName Dang nhap that bai '),
              Text('$userName Dang nhap  '),
              SizedBox(height: 24),
              ElevatedButton(
                  onPressed: () {
                    //loginModel.setFullName('Nguyen Van B');
                    validateLogin();
                    //Navigator.pushNamed(context, productListPage);
                    //Navigator.pushReplacementNamed(context, homePage);
                  },
                  child: Text('Login'))
            ],
          ),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);

    print("state = $state");
  }
}
