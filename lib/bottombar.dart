import 'package:flutter/material.dart';
import 'package:my_app3/product/page/product_list.dart';

import 'authen/page/login.dart';
import 'home/page/home_page.dart';

class MyBottomBarNavigator extends StatefulWidget {
  @override
  _MyBottomBarNavigatorState createState() => _MyBottomBarNavigatorState();
}

class _MyBottomBarNavigatorState extends State<MyBottomBarNavigator> {
  int currentIndex = 0;

  Widget callPage(int index) {
    switch (index) {
      case 0:
        return HomePage();
      case 1:
        return new ProductListPage();
      case 2:
        return LoginPage();
      case 3:
        return HomePage();
      default:
        return HomePage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (value) {
          setState(() {
            this.currentIndex = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.red,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Business',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'School',
            backgroundColor: Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            backgroundColor: Colors.pink,
          ),
        ],
      ),
    );
  }
}
