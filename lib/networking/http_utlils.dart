import 'dart:convert';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:my_app3/config/config.dart';

class ResponseApi {
  String? msg;
  int? code;
  dynamic? data;

  ResponseApi({this.msg, this.code, this.data});
}

class HttpUtils {
  static const String GET = "get";
  static const String POST = "post";
  ResponseApi? responseApi = new ResponseApi();

  Future<ResponseApi> request(
    String url,
    String method,
    Map<String, String> headers,
    Map<String, dynamic> params,
    // Function callback,
    // Function errorCallback
  ) async {
    // String msg;
    // int code;
    // var data;

    try {
      url = MyConfig.baseURL + url;

      Map<String, String> headerMap = headers == null ? new Map() : headers;
      Map<String, dynamic> parammap = params == null ? new Map() : params;
      // call api download/upload file => time

      // shared Preferences => key/value (key "acces_token")
      String accessToken = "";
      if (accessToken == null || accessToken.length == 0) {
      } else {
        headerMap["access_token"] = accessToken;
      }

      headerMap['Content-Type'] = 'application/json';

      http.Response res;

      if (POST == method) {
        res = await http.post(Uri.parse(url),
            headers: headerMap, body: jsonEncode(parammap));
      } else if (GET == method) {
        res = await http.get(Uri.parse(url), headers: headerMap);
      } else {
        res = await http.get(Uri.parse(url), headers: headerMap);
      }

      //String body = utf8.decode(res.bodyBytes);

      Map<String, dynamic> body = convert.jsonDecode(res.body);
      responseApi!.code = body['code'];
      responseApi!.msg = body['message'];
      responseApi!.data = body['data'];

      //callback(data);

    } catch (e) {
      // handle error
      print(e);
      // crashlytic firebase
      responseApi!.msg = "Co loi xay ra. Vui long thu lai sau";
      //errorCallback(e);
    }

    return responseApi!;
  }
}
