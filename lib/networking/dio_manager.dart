import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_app3/config/config.dart';

import 'http_utlils.dart';

enum Method { post, get, put, delete }

class DioManager {
  var dio = Dio();
  ResponseApi? responseApi = new ResponseApi();

  Future<dynamic> request({
    @required String? path,
    @required Method? method,
    Function(dynamic)? onSuccess,
    Function(dynamic)? onFailure,
    Map<String, dynamic>? params,
    Map<String, String>? headers,
  }) async {
    String? url;
    try {
      url = MyConfig.baseURL + path!;

      Map<String, String> headerMap = headers == null ? new Map() : headers;
      Map<String, dynamic> parammap = params == null ? new Map() : params;
      // call api download/upload file => time

      // shared Preferences => key/value (key "acces_token")
      String accessToken = "";
      if (accessToken == null || accessToken.length == 0) {
      } else {
        headerMap["access_token"] = accessToken;
      }

      headerMap['Content-Type'] = 'application/json';

      //Options? options;
      Response? response;

      switch (method) {
        case Method.get:
          response = await dio.get(url);
          break;
        case Method.post:
          response = await dio.post(url, data: parammap);
          break;
        case Method.put:
          response = await dio.put(url, data: parammap);
          break;
        case Method.delete:
          response = await dio.put(url, data: parammap);
          break;

        default:
      }

      print(response);

      if (response!.data is Map) {
        responseApi!.code = response.data['code'];
        responseApi!.msg = response.data['message'];
        responseApi!.data = response.data['data'];

        onSuccess!(responseApi);
      } else if (response.data is String) {
        onSuccess!(response.data);
      } else {
        onFailure!(jsonDecode("Co loi xay ra vui long thu lai sau"));
      }
    } on DioError catch (e) {
      print(e.message);
      print(e.error);
      print(e.type);
      onFailure!(jsonDecode("Co loi xay ra vui long thu lai sau"));
    }
  }
}
