import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:my_app3/authen/page/login.dart';
import 'package:my_app3/home/model/home_model.dart';
import 'package:my_app3/home/model/home_model_2.dart';
import 'package:my_app3/home/widget/service_home_widget.dart';
import 'package:my_app3/home/widget/slider_widget.dart';
import 'package:my_app3/product/page/product_list.dart';
import 'package:my_app3/route_manager.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeData? homeData;
  //List<HomeSlider> homeSliders = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  void getData() async {
    //var url = Uri.http('sanbongvinhloi.com', '/api/home');
    var url = Uri.parse('http://sanbongvinhloi.com/api/home');

    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      //print(jsonResponse);

      if (jsonResponse.isNotEmpty && jsonResponse["data"] != null) {
        setState(() {
          homeData = HomeData.fromJson(jsonResponse["data"]);
        });
      }
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  Widget searchBox(BuildContext context) {
    return Container(
      height: 32,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0), color: Colors.white),
      child: Row(
        children: [
          Padding(
              padding:
                  EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 15),
              child: Icon(
                Icons.search,
                size: 17,
                color: Colors.blue.shade600,
              )),
          Text(
            "Tìm kiếm sản phẩm",
            style: TextStyle(fontSize: 13, color: Colors.blue),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // leading: Image.network(
          //     "https://brasol.vn/public/ckeditor/uploads/thiet-ke-logo-tin-tuc/y-nghia-logo-tiki.jpg"),
          leading: Image.asset("assets/images/ic_logo_app.png"),
          title: searchBox(context),
          centerTitle: true,
          actions: [
            Image.asset(
              "assets/images/ic_near.png",
              scale: 3,
            ),
            Image.asset("assets/images/ic_near.png", scale: 3),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              sliderWidget(context, homeData!.sliders!),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Icon(Icons.security),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Dịch Vụ Cảnh Báo",
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              serviceHome(context, homeData!.services!),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Danh Muc San Pham",
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                    Icon(Icons.security),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  //height: 250,
                  child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2),
                    itemCount: topCategoryHome.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, productListPage);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Expanded(
                                  flex: 8,
                                  child: Image.network(
                                    topCategoryHome[index].image.toString(),
                                    fit: BoxFit.fitWidth,
                                  )),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 10),
                                  child: Text(
                                      topCategoryHome[index].name.toString(),
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                ),
                              )
                              //
                            ],
                          ),
                          width: MediaQuery.of(context).size.width,
                          //height: 200,
                        ),
                      );
                    },
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                  )),
            ],
          ),
        ));
  }
}
