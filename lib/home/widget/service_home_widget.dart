import 'package:flutter/material.dart';
import 'package:my_app3/home/model/home_model_2.dart';

Widget serviceHome(BuildContext context, List<HomeService> services) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 250,
    child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: services.length,
        itemBuilder: (context, index) {
          return Container(
            color: (index == 1) ? Colors.red : Colors.yellow,
            alignment: Alignment.center,
            child: Column(
              children: [
                Expanded(
                    flex: 8,
                    child: Image.network(
                      services[index].urlImageService.toString(),
                      fit: BoxFit.fitWidth,
                    )),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text("Quang cao",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                )
                //
              ],
            ),
            width: MediaQuery.of(context).size.width,
            height: 200,
          );
        }),
  );
}
