class HomeData {
  List<HomeSlider>? sliders;
  List<HomeService>? services;
  List<Category>? categories;

  HomeData({this.sliders, this.services, this.categories});

  factory HomeData.fromJson(Map<String, dynamic> json) => HomeData(
      sliders: List<HomeSlider>.from(
          json["sliders"].map((x) => HomeSlider.fromJson(x))),
      services: List<HomeService>.from(
          json["services"].map((x) => HomeService.fromJson(x))),
      categories: List<Category>.from(
          json["categories"].map((x) => Category.fromJson(x))));
}

class HomeSlider {
  int? idSlider;
  String? urlSlider;
  String? urlImageSlider;
  String? titleSlider;

  HomeSlider(
      {this.idSlider, this.urlSlider, this.urlImageSlider, this.titleSlider});

  factory HomeSlider.fromJson(Map<String, dynamic> json) => HomeSlider(
        idSlider: json["id"],
        urlSlider: json["url"],
        urlImageSlider: json["image"],
        titleSlider: json["title"],
      );
}

class HomeService {
  int? idService;
  String? urlService;
  String? urlImageService;
  String? titleService;

  HomeService(
      {this.idService,
      this.urlService,
      this.urlImageService,
      this.titleService});

  factory HomeService.fromJson(Map<String, dynamic> json) => HomeService(
        idService: json["id"],
        urlService: json["url"],
        urlImageService: json["image"],
        titleService: json["title"],
      );
}

class Category {
  int? idCat;
  String? nameCat;
  String? imageCat;
  String? descriptionCat;

  Category({this.idCat, this.nameCat, this.imageCat, this.descriptionCat});

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        idCat: json["category_id"],
        nameCat: json["name"],
        imageCat: json["image"],
        descriptionCat: json["description"],
      );
}
